import constant
import player
import pygame


class Window:

    def __init__(self):
        self.running = True
        self.screen = pygame.display.set_mode(constant.WINDOW_SIZE)
        pygame.display.set_caption('JumpGame')
        self.screen.fill(constant.BG_COLOR)

        self.player = player.Player(self.screen)

    def run(self):
        pygame.display.flip()

        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.player.left()
                        pygame.display.update()
                    elif event.key == pygame.K_RIGHT:
                        self.player.right()
                        pygame.display.update()
                    elif event.key == pygame.K_SPACE:
                        print('jump')
                if event.type == pygame.QUIT:
                    self.running = False


