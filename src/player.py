import pygame


class Player:

    def __init__(self, display):
        self.display = display
        self.color = (255, 0, 0)
        self.xPos = 50
        self.yPos = 50
        self.rect = pygame.draw.rect(self.display, self.color, (50, 50, self.xPos, self.yPos))

    def left(self):
        self.rect.move(-5, 0)

    def right(self):
        self.rect.move_ip(5, 0)
